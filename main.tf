module "web-prd" {
  source = "./modules/do_instance"

  size   = "${var.do_size}"
  region = "${var.do_region}"
  image  = "${var.do_image}"

  ssh_keys = "${var.do_ssh_keys}"

  hosts       = 1
  group       = "web"
  environment = "prd"
  zone        = "${var.cloudflare_zone}"
}

# module "web-tst" {
#   source = "./modules/instance"
#
#   ssh_keys    = "${var.hcloud_ssh_keys}"
#   location    = "${var.hcloud_location}"
#   type        = "${var.hcloud_type}"
#   image       = "${var.hcloud_image}"
#   hosts       = 1
#   group       = "web"
#   environment = "tst"
#   zone        = "${var.cloudflare_zone}"
# }
#
# module "web-dev" {
#   source = "./modules/instance"
#
#   ssh_keys    = "${var.hcloud_ssh_keys}"
#   location    = "${var.hcloud_location}"
#   type        = "${var.hcloud_type}"
#   image       = "${var.hcloud_image}"
#   hosts       = 1
#   group       = "web"
#   environment = "dev"
#   zone        = "${var.cloudflare_zone}"
# }
#
# module "cfg" {
#   source = "./modules/instance"
#
#   ssh_keys    = "${var.hcloud_ssh_keys}"
#   location    = "${var.hcloud_location}"
#   type        = "${var.hcloud_type}"
#   image       = "${var.hcloud_image}"
#   hosts       = 1
#   group       = "cfg"
#   environment = "mgt"
#   zone        = "${var.cloudflare_zone}"
# }
#
# module "svc" {
#   source = "./modules/instance"
#
#   ssh_keys    = "${var.hcloud_ssh_keys}"
#   location    = "${var.hcloud_location}"
#   type        = "${var.hcloud_type}"
#   image       = "${var.hcloud_image}"
#   hosts       = 1
#   group       = "svc"
#   environment = "mgt"
#   zone        = "${var.cloudflare_zone}"
# }
#
# module "ssh" {
#   source = "./modules/instance"
#
#   ssh_keys    = "${var.hcloud_ssh_keys}"
#   location    = "${var.hcloud_location}"
#   type        = "${var.hcloud_type}"
#   image       = "${var.hcloud_image}"
#   hosts       = 1
#   group       = "ssh"
#   environment = "mgt"
#   zone        = "${var.cloudflare_zone}"
# }
#
# module "mon" {
#   source = "./modules/instance"
#
#   ssh_keys    = "${var.hcloud_ssh_keys}"
#   location    = "${var.hcloud_location}"
#   type        = "${var.hcloud_type}"
#   image       = "${var.hcloud_image}"
#   hosts       = 1
#   group       = "mon"
#   environment = "mgt"
#   zone        = "${var.cloudflare_zone}"
# }
