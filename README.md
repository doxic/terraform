# <img src=".terraform.png" alt="Ansible" style="width:26px;"/> Terraform

This repository is used for deploying my infrastructure.

## Getting started
Installation is done with `pip` using Python 3 and venv. See the Links section for additional help installing Python 3.

Install git, git-crypt, python3 and pip3
```shell
$ sudo apt update && apt install git python3 python3-pip python3-setuptools git-crypt
```

Clone this repository locally
```shell
$ git clone https://gitlab.com/doxic/terraform.git
$ cd terraform
```

Create a virtual environment
```shell
$ python3 -m venv .venv
```

Activate virtual environment.
```shell
$ source .venv/bin/activate
```

Install project dependencies
```shell
$ pip install -r requirements.txt
```

Install Terraform
```shell
wget https://releases.hashicorp.com/terraform/0.12.0/terraform_0.12.0_linux_amd64.zip
unzip terraform*.zip
rm terraform*.zip
chmod +x terraform
sudo mv terraform /usr/bin
```

## Create User Account
* Head to [Add User](https://console.aws.amazon.com/iam/home?region=eu-west-1#/users$new?step=details) in AWS IAM
* Type in User Name `terraform` and enable **Programmatic access**
* Add permissions
  * AmazonEC2FullAccess
  * AmazonS3FullAccess
  * AmazonDynamoDBFullAccess
* Save in 1password or equal place

## Setup git-crypt
Configure a repository to use git-crypt (not required once initialized)
```shell
git-crypt init
```
List GPG keys
```shell
gpg --list-public-keys
```
Share the repository with others
```shell
git-crypt add-gpg-user --trusted 3884899842340B50
```
After cloning a repository with encrypted files, unlock with GPG
```shell
git-crypt unlock
```
List files for encryption
```shell
git-crypt status -e
```

## terraform.tfvars
```
cat << EOF > terraform.tfvars
hcloud_token = ""
cloudflare_email = ""
cloudflare_token = ""
EOF
```


## Run

Create server by running:

TF_VAR_hcloud_token=YOUR_SECRET_TOKEN
terraform init
terraform plan
terraform apply
terraform destroy

## Providers
### Cloudflare
https://www.terraform.io/docs/providers/cloudflare/index.html
https://blog.cloudflare.com/getting-started-with-terraform-and-cloudflare-part-1/


## Provision with Ansible
```
wget  https://raw.githubusercontent.com/hg8496/ansible-hcloud-inventory/master/hcloud.py
chmod +x hcloud.py
export HCLOUD_TOKEN=example
```
Run ansible ping
```
"tag_App_backend:&tag_Environment_staging:&tag_Usage_clock_worker" -m ping all
ansible -i ./hcloud.py -m ping group-webserver
```

[Finding a Quick Start AMI](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/finding-an-ami.html#finding-an-ami-aws-cli)
https://www.digitalocean.com/community/tutorials/how-to-use-terraform-with-digitalocean
https://docs.hetzner.cloud/


## Links
- Remote State
  - TERRAFORM AWS BACKEND FOR REMOTE STATE FILES WITH S3 AND DYNAMODB: http://aws-cloud.guru/terraform-aws-backend-for-remote-state-files-with-s3-and-dynamodb/
  - Terraform workspaces and locals for environment separation: https://medium.com/@diogok/terraform-workspaces-and-locals-for-environment-separation-a5b88dd516f5
  - Secrets Management with Terraform:  https://www.linode.com/docs/applications/configuration-management/secrets-management-with-terraform/
  - An Introduction to Terraform: https://blog.gruntwork.io/an-introduction-to-terraform-f17df9c6d180
  - https://www.redpill-linpro.com/techblog/2018/08/14/getting-started-with-terraform.html
- Modules
  - Create a Terraform Module:  https://www.linode.com/docs/applications/configuration-management/create-terraform-module/
- Gitlab
  - https://medium.com/@timhberry/terraform-pipelines-in-gitlab-415b9d842596
- Hetzner Cloud
  - API Docs: https://docs.hetzner.cloud/
