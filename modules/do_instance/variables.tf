variable "group" {
  description = "Server Group Name"
}

variable "environment" {
  description = "Server Environment Name"
  type        = "string"
}

variable "hosts" {
  default = 0
}

variable "size" {
  description = "The unique slug that indentifies the type of Droplet"
  default     = "s-1vcpu-1gb"
}

variable "region" {
  description = "The region to start in"
  default     = "fra1"
}

variable "image" {
  description = "The Droplet image ID"
  default     = "centos-7-x64"
}

variable "ssh_keys" {
  type        = "list"
  description = "SSH Key Name"
}

variable "zone" {
  description = "Cloudflare Domain Zone"
  type        = "string"
}
