# https://www.terraform.io/docs/providers/do/r/droplet.html
resource "digitalocean_droplet" "host" {
  name   = "${var.group}${count.index + 1}-${var.environment}-${terraform.workspace}"
  size   = "${var.size}"
  region = "${var.region}"
  image  = "${var.image}"

  backups            = false
  private_networking = true
  ssh_keys           = "${var.ssh_keys}"

  count = "${var.hosts}"

  tags = [
    "${digitalocean_tag.group.name}",
    "${digitalocean_tag.environment.name}",
    "${digitalocean_tag.workspace.name}"
  ]
}

resource "digitalocean_tag" "group" {
  name = "${var.group}"
}
resource "digitalocean_tag" "environment" {
  name = "${var.environment}"
}
resource "digitalocean_tag" "workspace" {
  name = "${terraform.workspace}"
}

resource "cloudflare_record" "dns" {
  domain  = "${var.zone}"
  name    = "${digitalocean_droplet.host[count.index].name}"
  value   = "${digitalocean_droplet.host[count.index].ipv4_address}"
  type    = "A"
  proxied = false

  count = "${var.hosts}"
}
