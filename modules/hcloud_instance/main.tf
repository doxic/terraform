resource "hcloud_server" "host" {
  name        = "${var.group}${count.index + 1}-${var.environment}-${terraform.workspace}"
  location    = "${var.location}"
  image       = "${var.image}"
  server_type = "${var.type}"
  ssh_keys    = "${var.ssh_keys}"

  count = "${var.hosts}"

  labels = {
    "group"       = "${var.group}"
    "environment" = "${var.environment}"
    "workspace"   = "${terraform.workspace}"
  }
}

resource "cloudflare_record" "dns" {
  domain  = "${var.zone}"
  name    = "${hcloud_server.host[count.index].name}"
  value   = "${hcloud_server.host[count.index].ipv4_address}"
  type    = "A"
  proxied = false

  count = "${var.hosts}"
}
