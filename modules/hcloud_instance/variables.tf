variable "group" {
  description = "Server Group Name"
}

variable "environment" {
  description = "Server Environment Name"
  type        = "string"
}

variable "hosts" {
  default = 0
}

variable "location" {
  type = "string"
}

variable "type" {
  type        = "string"
  description = "Name of the Hetzner Cloud server type"
  default     = "cx11"
}

variable "image" {
  type        = "string"
  description = "Hetzner Cloud Image"
  default     = "centos-7"
}

variable "ssh_keys" {
  type        = "list"
  description = "SSH Key Name"
}

variable "zone" {
  description = "Cloudflare Domain Zone"
  type        = "string"
}
