variable "aws_access_key" {
  description = "AWS API access key"
}

variable "aws_secret_key" {
  description = "AWS API secret key"
}

variable "aws_region" {
  description = "AWS region"
  default     = "eu-west-1"
}

variable "hcloud_token" {
  description = "Hetzner Cloud API token"
}

variable "do_token" {
  description = "DigitalOcean Personal Access Token"
}

variable "do_ssh_keys" {
  description = "DigitalOcean Public Keys"
  default     = []
}

variable "cloudflare_email" {
  description = "Cloudflare email address"
}

variable "cloudflare_token" {
  description = "Cloudflare API token"
}

variable "hcloud_ssh_keys" {
  description = "SSH Key Name"
  default     = []
}

variable "hcloud_image" {
  description = "Hetzner Cloud Image"
  default     = "centos-7"
}

variable "hcloud_server_type" {
  description = "Name of the Hetzner Cloud server type"
  default     = "cx11"
}

variable "hcloud_location" {
  default = "nbg1"
}

variable "hcloud_type" {
  default = "cx11"
}

variable "cloudflare_zone" {
  description = "Cloudflare Zone resource"
}

variable "do_size" {
  description = "The unique slug that indentifies the type of Droplet"
  default     = "s-1vcpu-1gb"
}

variable "do_region" {
  description = "The region to start in"
  default     = "fra1"
}

variable "do_image" {
  description = "The Droplet image ID"
  default     = "centos-7-x64"
}
